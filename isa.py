import json
from enum import Enum


class Opcode(str, Enum):
    """Opcode для инструкций."""

    NOP = "nop"
    HLT = "hlt"

    INC = "inc"
    DEC = "dec"
    ADD = "add"
    SUB = "sub"
    MOD = "mod"
    ST = "st"
    LD = "ld"
    MOV = "mov"
    AND_ = "and"

    JG = "jg"
    JZ = "jz"
    JNZ = "jnz"
    JMP = "jmp"

    def __str__(self) -> str:
        return str(self.value)


branch_instructions = [Opcode.JG, Opcode.JZ, Opcode.JNZ, Opcode.JMP]

two_parameter_instructions = [Opcode.ADD, Opcode.SUB, Opcode.MOD, Opcode.AND_, Opcode.ST, Opcode.LD, Opcode.MOV]

one_parameter_instructions = [Opcode.INC, Opcode.DEC]

zero_parameters_instructions = [Opcode.NOP, Opcode.HLT]


def write_code(filename: str, code):
    with open(filename, "w", encoding="utf-8") as file:
        buf = []
        for instr in code:
            buf.append(json.dumps(instr))
        file.write("[" + ",\n ".join(buf) + "]")


def read_code(filename: str):
    with open(filename, encoding="utf-8") as file:
        return json.loads(file.read())


def is_integer(value: str) -> bool:
    if value is None:
        return False
    try:
        int(value)
    except ValueError:
        return False
    else:
        return True


def get_def():
    return {"opcode": "nop", "arg_1": "0", "is_indirect_1": False, "arg_2": None, "is_indirect_2": None}
