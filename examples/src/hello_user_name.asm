first_message:
  .word 'What is your name?'

second_message:
  .word 'Hello, '

first_message_pointer:
  .word first_message

second_message_pointer:
  .word second_message

read_name_pointer:
  .word name

name_length:
  .word 0

write_name_pointer:
  .word name

end_str:
  .word 10

in_addr:
  .word 1

out_addr:
  .word 2

exclamation_point:
  .word 33

_start:

  write_first_message:
    ld r3, first_message
    ld r2, first_message_pointer
    and r1, r1
    first_loop:
    dec r3
    inc r2
    st r2, first_message_pointer
    ld r0, (first_message_pointer)
    st r0, (out_addr)
    sub r3, r1
    jnz first_loop
    jmp read_name

  read_name:
    read_name_char:
      ld r0, (in_addr)
      ld r3, name_length
      inc r3
      st r3, name_length

    store_name_char:
      st r0, (read_name_pointer)
      ld r1, end_str
      sub r0, r1
      jz write_second_message
      ld r0, read_name_pointer
      inc r0
      st r0, read_name_pointer
      jmp read_name_char

  write_second_message:
    ld r3, second_message
    ld r2, second_message_pointer
    and r1, r1
    second_loop:
    dec r3
    inc r2
    st r2, second_message_pointer
    ld r0, (second_message_pointer)
    st r0, (out_addr)
    sub r3, r1
    jnz second_loop

  write_name:
    ld r3, name_length
    dec r3
    ld r2, write_name_pointer
    and r1, r1
    name_loop:
    dec r3
    ld r0, (write_name_pointer)
    st r0, (out_addr)
    inc r2
    st r2, write_name_pointer
    sub r3, r1
    jnz name_loop

  write_exclamation_point:
    ld r0, exclamation_point
    st r0, (out_addr)
    hlt
name:
  .word 0