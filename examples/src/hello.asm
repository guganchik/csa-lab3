message:
  .word 'Hello world!'

pointer:
  .word message

end_str:
  .word 0

out_addr:
  .word 2

_start:
  ld r3, message
  dec r3
  ld r2, pointer
  inc r2
  st r2, pointer
  ld r0, (pointer)
  st r0, (out_addr)
  sub r3, 0
  jnz _start
  hlt