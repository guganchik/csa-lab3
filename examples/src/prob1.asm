max_number:
  .word 1000

first_del:
  .word 3

second_del:
  .word 5

out_addr:
  .word 3

_start:
  loop:
    inc r1
    mov r2, r1
    ld r3, max_number
    sub r2, r3
    jz write_answer
    mov r2, r1
    ld r3, first_del
    mod r2, r3
    jz add_number
    mov r2, r1
    ld r3, second_del
    mod r2, r3
    jz add_number
    jmp loop

  add_number:
    add r0, r1
    jmp loop

  write_answer:
    st r0, (out_addr)
    hlt
