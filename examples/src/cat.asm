end_str:
  .word 10

in_addr:
  .word 1

out_addr:
  .word 2

_start:
  ld r0, (in_addr)
  mov r1, r0
  ld r2, end_str
  sub r1, r2
  jnz write
  hlt

write:
  st r0, (out_addr)
  jmp _start