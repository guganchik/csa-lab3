from __future__ import annotations

import logging
import sys
from typing import Callable

import isa


class ALU:
    def __init__(self):
        self.n = False
        self.z = True

    def __str__(self):
        return f"N: {self.n}, Z: {self.z}"

    def get_flags(self) -> tuple[bool, bool]:
        return self.n, self.z

    def set_flags(self, val: int):
        self.n = val < 0
        self.z = val == 0

    def do_operation(self, left: int, right: int, func: Callable[[int, int], int]) -> int:
        res = func(left, right)
        self.set_flags(res)
        return res


class Datapath:
    def __init__(self, memory, input_buffer):
        self.alu = ALU()
        self.memory_size = 65536
        self.memory: list[dict[str, str | int | bool | None]] = [isa.get_def()] * 65536
        self.fill_memory(memory)
        self.input_buffer: list = input_buffer
        self.output_str_buffer = []
        self.output_int_buffer = []
        self.registers = [0, 0, 0, 0]
        self.ir = {}
        self.dr = {}
        self.ar = 0
        self.left = 0
        self.right = 0

    def fill_memory(self, memory):
        for cell in memory:
            index = cell["index"]
            cell.pop("index")
            self.memory[index] = cell
        self.memory[1:4] = isa.get_def(), isa.get_def(), isa.get_def()

    def read_mem(self) -> dict[str, str]:
        if self.ar == 1:
            logging.debug("INPUT: %s", self.input_buffer[0])
            val = self.input_buffer.pop(0)
            return {
                "opcode": isa.Opcode.NOP.value,
                "arg_1": val if isinstance(val, int) else ord(val),
                "is_indirect_1": False,
                "arg_2": None,
                "is_indirect_2": None,
            }
        return self.memory[self.ar]

    def write_mem(self, val: int):
        if self.ar == 2:
            logging.debug("OUTPUT char: %s <- %s", "".join(self.output_str_buffer), chr(val))
            self.output_str_buffer.append(chr(val))
        elif self.ar == 3:
            logging.debug("OUTPUT int: %s <- %s", ", ".join(self.output_int_buffer), val)
            self.output_int_buffer.append(val)
        else:
            t = {
                "opcode": isa.Opcode.NOP.value,
                "arg_1": str(val),
                "is_inderect_1": False,
                "arg_2": None,
                "is_inderect_2": None,
            }
            self.memory[self.ar] = t

    def latch_dr(self):
        self.dr = self.read_mem()

    def latch_ir(self):
        self.ir = self.read_mem()


class ControlUnit:
    def __init__(self, datapath: Datapath, limit: int):
        self.datapath = datapath
        self.limit = limit
        self.tick = 0
        self.instr_cnt = 0
        self.pc = 0

    def command_cycle(self):
        try:
            while self.instr_cnt <= self.limit:
                self.instr_cnt += 1
                self.instruction_fetch()
                self.operand_fetch()
                self.execute()
                logging.debug("%s", self)
        except StopIteration:
            logging.debug("%s", self)

    def instruction_fetch(self):
        self.tick += 1
        self.datapath.ar = self.pc
        self.datapath.latch_ir()
        self.pc += 1

    def operand_fetch(self):
        if self.datapath.ir["opcode"] in ["hlt", "nop"] or self.datapath.ir["arg_1"] is None:
            return
        if self.datapath.ir["opcode"] in ["jg", "jz", "jnz", "jmp"]:
            self.datapath.left = int(self.datapath.ir["arg_1"])
        elif not isa.is_integer(self.datapath.ir["arg_1"]):
            self.datapath.left = self.datapath.registers[int(self.datapath.ir["arg_1"][1:])]
        if self.datapath.ir["is_indirect_2"] is None:
            return
        if not isa.is_integer(self.datapath.ir["arg_2"]):
            self.datapath.right = self.datapath.registers[int(self.datapath.ir["arg_2"][1:])]
            return
        if self.datapath.ir["is_indirect_2"]:
            self.tick += 1
            self.datapath.ar = int(self.datapath.ir["arg_2"])
            self.datapath.latch_dr()
            self.tick += 1
            self.datapath.ar = int(self.datapath.dr["arg_1"])
            self.datapath.latch_dr()
            self.datapath.right = int(self.datapath.dr["arg_1"])
        else:
            self.tick += 1
            self.datapath.ar = int(self.datapath.ir["arg_2"])
            self.datapath.latch_dr()
            self.datapath.right = int(self.datapath.dr["arg_1"])

    def write_alu_out(self, val: int):
        self.datapath.registers[int(self.datapath.ir["arg_1"][1:])] = val

    def execute(self):
        if self.datapath.ir["opcode"] == isa.Opcode.HLT.value:
            raise StopIteration
        self.tick += 1
        match self.datapath.ir["opcode"]:
            case isa.Opcode.ADD:
                res = self.datapath.alu.do_operation(self.datapath.left, self.datapath.right, lambda x, y: x + y)
                self.write_alu_out(res)
            case isa.Opcode.SUB:
                res = self.datapath.alu.do_operation(self.datapath.left, self.datapath.right, lambda x, y: x - y)
                self.write_alu_out(res)
            case isa.Opcode.INC:
                res = self.datapath.alu.do_operation(self.datapath.left, 0, lambda x, _: x + 1)
                self.write_alu_out(res)
            case isa.Opcode.DEC:
                res = self.datapath.alu.do_operation(self.datapath.left, 0, lambda x, _: x - 1)
                self.write_alu_out(res)
            case isa.Opcode.MOD:
                res = self.datapath.alu.do_operation(self.datapath.left, self.datapath.right, lambda x, y: x % y)
                self.write_alu_out(res)
            case isa.Opcode.ST:
                self.datapath.write_mem(self.datapath.left)
            case isa.Opcode.LD:
                r = int(self.datapath.ir["arg_1"][1:])
                self.datapath.registers[r] = self.datapath.right
            case isa.Opcode.MOV:
                fr = int(self.datapath.ir["arg_1"][1:])
                sr = int(self.datapath.ir["arg_2"][1:])
                self.datapath.registers[fr] = self.datapath.registers[sr]
            case isa.Opcode.AND_:
                res = self.datapath.alu.do_operation(self.datapath.left, 0, lambda x, y: x & y)
                self.write_alu_out(res)
            case isa.Opcode.JMP:
                self.pc = self.datapath.left
            case isa.Opcode.JNZ:
                if not self.datapath.alu.get_flags()[1]:
                    self.pc = self.datapath.left
            case isa.Opcode.JZ:
                if self.datapath.alu.get_flags()[1]:
                    self.pc = self.datapath.left
            case isa.Opcode.JG:
                if not self.datapath.alu.get_flags()[0]:
                    self.pc = self.datapath.left

    def __repr__(self):
        return (
            f"Tick: {self.tick} | r0: {self.datapath.registers[0]}, r1: {self.datapath.registers[1]} "
            f"r2: {self.datapath.registers[2]}, r3: {self.datapath.registers[3]}, pc: {self.pc} "
            f"ar: {self.datapath.ar}, dr: {self.datapath.dr.get('arg_1')} | Flags: {self.datapath.alu} "
            f"Instruction: {self.datapath.ir['opcode']} {self.datapath.ir['arg_1']} {self.datapath.ir['arg_2']}"
        )


def simulation(code, input_tokens):
    datapath = Datapath(code, input_tokens)
    control_unit = ControlUnit(datapath, 15000)
    control_unit.command_cycle()
    logging.debug("output_str_buffer: %s", "".join(control_unit.datapath.output_str_buffer))
    logging.debug("output_int_buffer: %s", ", ".join(map(str, control_unit.datapath.output_int_buffer)))
    print(f"instr_counter: {control_unit.instr_cnt}, ticks: {control_unit.tick}")


def main(code_file, input_file):
    code = isa.read_code(code_file)
    with open(input_file) as file:
        text = file.read()
        if text:
            input_tokens = eval(text)
        else:
            input_tokens = []

    simulation(code, input_tokens)
    pass


if __name__ == "__main__":
    assert len(sys.argv) == 3, "Wrong args: memory_file, input_file"
    logging.getLogger().setLevel(logging.DEBUG)
    _, code_file, input_file = sys.argv
    main(code_file, input_file)
